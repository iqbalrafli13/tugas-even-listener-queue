<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Blog Anda Berhasil di posting</title>
    </head>
    <body>

        <h2>hai Saudara/i {{$nama}} ,</h2>
        <p>Ada blog baru dari {{$penulis}} yang berjudul {{$judul}} </p>
        <p>Silahkan review   <a href="{{url('localhost:8000/api/review/'.$id)}}"> di sini </a></p>
        <p>dan publish <a href="{{url('localhost:8000/api/publish/'.$id)}}"> di sini </a> </p>
        <hr>
        <h5>{{$judul}}</h5>
        <p>{{$content}}</p>

    </body>
</html>
