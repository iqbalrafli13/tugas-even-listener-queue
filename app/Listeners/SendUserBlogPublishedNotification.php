<?php

namespace App\Listeners;

use App\Events\BlogPublishedEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\BlogPublished;
use Mail;

class SendUserBlogPublishedNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BlogPublishedEvent  $event
     * @return void
     */
    public function handle(BlogPublishedEvent $event)
    {
        Mail::to($event->blog->user)->send(new BlogPublished($event->blog));
    }
}
