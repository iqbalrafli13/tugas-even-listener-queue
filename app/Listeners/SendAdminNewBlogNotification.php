<?php

namespace App\Listeners;

use App\Events\BlogPostedEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\AdminBlogPosted;
use Mail;
use App\User;
class SendAdminNewBlogNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BlogPostedEvent  $event
     * @return void
     */
    public function handle(BlogPostedEvent $event)
    {
        $users = User::where('role', 1)->get();
        foreach ($users as $user) {
            Mail::to($user)->send(new AdminBlogPosted($event->blog, $user));
        }
    }
}
