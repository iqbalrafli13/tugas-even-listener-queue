<?php

namespace App\Listeners;

use App\Events\BlogPostedEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\UserBlogPosted;
use Mail;
class SendUserBlogPostedNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BlogPostedEvent  $event
     * @return void
     */
    public function handle(BlogPostedEvent $event)
    {
        Mail::to($event->blog->user)->send(new UserBlogPosted($event->blog));
    }
}
