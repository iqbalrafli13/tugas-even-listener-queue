<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\Blog;
use App\Events\BlogPostedEvent;
use App\Events\BlogPublishedEvent;

class BlogController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request,["title"=>"required", "content"=>"required",]);

        $blog = $request->user()->blogs()->create([
            'title' => $request->json('title'),
            'content'=> $request->json('content'),
        ]);
        event(new BlogPostedEvent($blog));
        return response()->json([$blog], 200);

    }

    public function publish(Request $request, $id)
    {
        if (Auth()->user()->isAdmin()) {
            $blog = Blog::find($id);
            $blog->update(["publish"=>1]);

            event(new BlogPublishedEvent($blog));

            return response()->json(["messages"=>"Blog berhasil di publish"], 200);
        }else {
            return response()->json(["messages"=>"Forbiden"], 403);
        }
    }
    public function review(Request $request, $id)
    {
        if (Auth()->user()->isAdmin()) {
            $blog = Blog::find($id);

            return response()->json([
                "title"=>$blog->title,
                "content"=>$blog->content,
                "publish"=>$blog->publish
            ], 200);
        }else {
            return response()->json(["messages"=>"Forbiden"], 403);
        }
    }
}
