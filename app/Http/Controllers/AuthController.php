<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use JWTAuth;
// use App\Mail\BlogPosted;
// use Mail;
// use App\Events\BlogPostedEvent;


class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */

     public function me()
     {
         return response()->json($this->guard()->user());
     }
    public function signup(Request $request)
    {
        $this->validate($request,[
            "name"=>"required",
            "email"=>"required|unique:users",
            "password"=>"required",
        ]);

        $user = User::create([
            "name"=>$request->json("name"),
            "email"=>$request->json("email"),
            "password"=>bcrypt($request->json("password"))
        ]);

        return response()->json([
            "name"=>$request->json("name"),
            "email"=>$request->json("email"),
        ], 200);
    }

    public function signin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required', 'password' => 'required',
        ]);

        // grab credentials from the request
        $credentials = $request->only('email', 'password');

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        // all good so return the token
        return response()->json([
            'user_id' => $request->user()->id,
            'token'   => $token
        ]);

    }

    /**
    * Get the guard to be used during authentication.
    *
    * @return \Illuminate\Contracts\Auth\Guard
    */
    public function guard()
    {
        return Auth::guard();
    }

}
