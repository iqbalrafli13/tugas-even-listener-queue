<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\User;
use App\Blog;

class AdminBlogPosted extends Mailable
{
    use Queueable, SerializesModels;
    protected $blog;
    protected $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
     public function __construct(Blog $blog,User $user)
     {
         $this->blog = $blog;
         $this->user = $user;
     }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('example@example.com')
                    ->view('email.admin_blog_posted')
                    ->with([
                        "nama"=>$this->user->name,
                        "penulis"=>$this->blog->user->name,
                        "judul"=>$this->blog->user->title,
                        "content"=>$this->blog->user->content,
                        "id"=>$this->blog->user->id,
                    ]);
    }
}
