<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('signup', 'AuthController@signup');
    Route::post('signin', 'AuthController@signin');
    Route::post('signout', 'AuthController@signout');
});

Route::group(['middleware' => ['jwt.auth']], function(){
    Route::post('/tulis_blog', 'BlogController@store');
    Route::post('/review/{id}', 'BlogController@review');
    Route::post('/publish/{id}', 'BlogController@publish');
});
